﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Forms;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;


namespace Flow_Cal_Commit
{
    public partial class Form1 : Form
    {
        private string Flow_Cal_Output_Folder1;
        public Form1()
        {
            InitializeComponent();
            lblFlowCalOutputFolder.Text = "C:\\";
        }

        private void cmdBuildFlowCalFile_Click(object sender, EventArgs e)
        {
            List<Flow_Cal_Analysis_Entry> Alist = new List<Flow_Cal_Analysis_Entry>();
            List<Flow_Cal_Extended_Entry> Xlist = new List<Flow_Cal_Extended_Entry>();
            List<Flow_Cal_Extended_Chars_Entry> Wlist = new List<Flow_Cal_Extended_Chars_Entry>();
            Flow_Cal_Analysis_Record Arecord;
            Arecord = new Flow_Cal_Analysis_Record();
            Flow_Cal_Extended_Record Xrecord;
            string Astring;
            string Xstring;
            string Wstring;
            string FileName;
            string SampleDate;
            Xrecord = new Flow_Cal_Extended_Record();
            Astring = "";
            Xstring = "";
            Wstring = "";
            FileName = "Arecord.txt";
            frmWritingFlowCalFile WritingFlowCalFile = new frmWritingFlowCalFile();
            WritingFlowCalFile.Show();
            try
            {
                myExcelApp = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                myExcelWorkbooks = myExcelApp.Workbooks;
                myExcelWorkBook = myExcelApp.Workbooks.get_Item(1);
                myExcelWorksheet = (_Worksheet)myExcelWorkBook.Worksheets.get_Item("CONFIG");
            }
            catch
            {
                MessageBox.Show("Calculations.xls is not started!", "Flow Cal Error");
                System.Windows.Forms.Application.Exit();
            }
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[4, 19], myExcelWorksheet.Cells[4, 19]);
            SampleDate = DateTime.FromOADate(Convert.ToDouble(xlrange.Value2));
            if (txtSampleID.Text.CompareTo("") != 0)
            {
                SampleDate = dtpSampleDateTime.Value.Month.ToString() + dtpSampleDateTime.Value.Day.ToString() + dtpSampleDateTime.Value.Hour.ToString() + dtpSampleDateTime.Value.Minute.ToString();
                FileName = txtSourceAnalysisNumber.Text + txtSampleID.Text + SampleDate + ".txt";
            }
            Alist = Fill_Analysis_Record();
            Astring = Build_Astring(Alist);
            TextWriter tw = new StreamWriter(lblFlowCalOutputFolder.Text + FileName, false);
            tw.WriteLine(Astring);
            tw.Close();
            if(rdoExtYes.Checked == true)
            {
                Xlist = Fill_Extended_Record();
                Xstring = Build_Xstring(Xlist);
                tw = new StreamWriter(lblFlowCalOutputFolder.Text + FileName, true);
                tw.WriteLine(Xstring);
                tw.Close();
            }
            if (rdoWYes.Checked == true)
            {
                Wlist = Fill_Extended_Chars_Record();
                Wstring = Build_Wstring(Wlist);
                tw = new StreamWriter(lblFlowCalOutputFolder.Text + FileName, true);
                tw.WriteLine(Wstring);
                tw.Close();
            }
            WritingFlowCalFile.Hide();
            MessageBox.Show("Flow-Cal Filename is " + FileName, "Flow-Cal File Name");
        }


        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private List<Flow_Cal_Analysis_Entry> Fill_Analysis_Record()
        {
            Microsoft.Office.Interop.Excel.Application myExcelApp;
            Workbooks myExcelWorkbooks;
            Workbook myExcelWorkBook = null;
            _Worksheet myExcelWorksheet = null;
            Microsoft.Office.Interop.Excel.Range xlrange;
            List<Flow_Cal_Analysis_Entry> Alist = new List<Flow_Cal_Analysis_Entry>();
            Flow_Cal_Analysis_Entry AEntry = new Flow_Cal_Analysis_Entry();
            Flow_Cal_Analysis_Record Arecord;
            object misValue = System.Reflection.Missing.Value; //Missing value object for the excel routines
            Arecord = new Flow_Cal_Analysis_Record();
            AEntry.name = "SourceAnalysisNumber";
            AEntry.value = txtSourceAnalysisNumber.Text;
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            Arecord.SourceAnalysisNumber = txtSourceAnalysisNumber.Text + "\t";
            AEntry.name = "ColumnType";
            AEntry.value = "A\t";
            Alist.Add(AEntry);
            Arecord.ColumnType = "A\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "FromDate";
            AEntry.value = dtpFromdate.Value + "\t";
            Alist.Add(AEntry);
            Arecord.FromDate = dtpFromdate.Value + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "ToDate";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            Arecord.ToDate = "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SampleDate";
            AEntry.value = dtpSampleDateTime.Value + "\t";
            Alist.Add(AEntry);
            Arecord.SampleDate = dtpSampleDateTime.Value + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "Sampletype";
            AEntry.value = "S\t";
            Alist.Add(AEntry);
            Arecord.SampleDate = "S\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "BTUBase";
            AEntry.value = "D\t";
            Alist.Add(AEntry);
            Arecord.BTUBase = "D\t";
            try
            {
                myExcelApp = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
                myExcelWorkbooks = myExcelApp.Workbooks;
                myExcelWorkBook = myExcelApp.Workbooks.get_Item(1);
                myExcelWorksheet = (_Worksheet)myExcelWorkBook.Worksheets.get_Item("CALCULATIONS");
            }
            catch 
            {
                MessageBox.Show("Calculations.xls is not started!", "Flow Cal Error");
                System.Windows.Forms.Application.Exit();
            }
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 22], myExcelWorksheet.Cells[239, 22]);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 19], myExcelWorksheet.Cells[239, 19]);
            Arecord.HeatingValue = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "HeatingValue";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 30], myExcelWorksheet.Cells[239, 30]);
            Arecord.RelativeDensity = string.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "RelativeDensity";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "Viscosity";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SpecificHeatRatio";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[242, 9], myExcelWorksheet.Cells[242, 9]);
            Arecord.PressureBase = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PressureBase";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[3, 10], myExcelWorksheet.Cells[3, 10]);
            Arecord.molpercentCO2 = String.Format("0:00.###", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentCO2";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[8, 10], myExcelWorksheet.Cells[8, 10]);
            Arecord.molpercentN2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentN2";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[11, 10], myExcelWorksheet.Cells[11, 10]);
            Arecord.molpercentMethane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentMethane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[12, 10], myExcelWorksheet.Cells[12, 10]);
            Arecord.molpercentEthane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentEthane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[14, 10], myExcelWorksheet.Cells[14, 10]);
            Arecord.molpercentPropane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentPropane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[15, 10], myExcelWorksheet.Cells[15, 10]);
            Arecord.molpercentIsobutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentIsobutane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[16, 10], myExcelWorksheet.Cells[16, 10]);
            Arecord.molpercentNButane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentNButane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[18, 10], myExcelWorksheet.Cells[18, 10]);
            Arecord.molpercentIsopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentIsopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[19, 10], myExcelWorksheet.Cells[19, 10]);
            Arecord.molpercentNPentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentNPentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[20, 10], myExcelWorksheet.Cells[20, 10]);
            Arecord.molpercentneoPentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentneoPentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[27, 10], myExcelWorksheet.Cells[27, 10]);
            Arecord.molpercentHexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentHexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[45, 10], myExcelWorksheet.Cells[45, 10]);
            Arecord.molpercentHeptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentHeptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[68, 10], myExcelWorksheet.Cells[68, 10]);
            Arecord.molpercentOctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentOctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[72, 10], myExcelWorksheet.Cells[72, 10]);
            Arecord.molpercentNonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentNonane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[73, 10], myExcelWorksheet.Cells[73, 10]);
            Arecord.molpercentDecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentDecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[7, 10], myExcelWorksheet.Cells[7, 10]);
            Arecord.molpercentArgon = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentArgon";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[2, 10], myExcelWorksheet.Cells[2, 10]);
            Arecord.molpercentCO = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentCO";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[5, 10], myExcelWorksheet.Cells[5, 10]);
            Arecord.molpercentH2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentH2";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[6, 10], myExcelWorksheet.Cells[6, 10]);
            Arecord.molpercentO2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentO2";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[9, 10], myExcelWorksheet.Cells[9, 10]);
            Arecord.molpercentH2O = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentH2O";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[13, 10], myExcelWorksheet.Cells[13, 10]);
            Arecord.molpercentH2S = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentH2S";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[10, 10], myExcelWorksheet.Cells[10, 10]);
            Arecord.molpercentHe = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentHe";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PPMH2S";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PPMCOS";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PPMMeSH";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PPMMES";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PPMEtSH";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PPMDMS";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PPMSulfurs";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "LbpermillioncuftH2O";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 66], myExcelWorksheet.Cells[239, 66]);
            Arecord.GPMMeasured = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "GPMMeasured";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            Arecord.Samplenumber = txtSampleNumber.Text;
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "Samplenumber";
            AEntry.value = txtSampleNumber.Text + "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 22], myExcelWorksheet.Cells[239, 22]);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 19], myExcelWorksheet.Cells[239, 19]);
            Arecord.HeatingValue1 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "HeatingValue1";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[266, 9], myExcelWorksheet.Cells[266, 9]);
            Arecord.GPMCondensate = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "GPMCondensate";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[274, 9], myExcelWorksheet.Cells[274, 9]);
            Arecord.BTUC6plus = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "BTUC6plus";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            Arecord.FieldRemarks = txtFieldComments.Text + "\t";
            Arecord.OfficeRemarks = txtOfficeComments.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "FieldRemarks";
            AEntry.value = txtFieldComments.Text + "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "OfficeRemarks";
            AEntry.value = txtOfficeComments.Text + "\t";
            Alist.Add(AEntry);
            Arecord.SampleTechnician = txtSampleTechnician.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SampleTechnician";
            AEntry.value = txtSampleTechnician.Text + "\t";
            Alist.Add(AEntry);
            Arecord.AnalysisTechnician = txtAnalysisTechnician.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "AnalysisTechnician";
            AEntry.value = txtAnalysisTechnician.Text + "\t";
            Alist.Add(AEntry);
            Arecord.InstrumentNumber = txtInstrumentNumber.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "InstrumentNumber";
            AEntry.value = txtInstrumentNumber.Text + "\t";
            Alist.Add(AEntry);
            Arecord.SamplePressure = txtSamplePressure.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SamplePressure";
            AEntry.value = txtSamplePressure.Text + "\t";
            Alist.Add(AEntry);
            Arecord.SampleTemperature = txtSampleTemperature.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SampleTemperature";
            AEntry.value = txtSampleTemperature.Text + "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinednumber1";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinednumber2";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinednumber3";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinednumber4";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinednumber5";
            AEntry.value = mseH2S.Text + "\t";
            //AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinedstring1";
            if ((cbSayboltColor.Text != "") && (cbSayboltColor.Text != "None"))
            {
                AEntry.value = cbSayboltColor.Text + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinedstring2";
            if ((cbCopperStrip.Text != "") && (cbCopperStrip.Text != "None"))
            {
                AEntry.value = cbCopperStrip.Text;
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinedstring3";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinedstring4";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinedstring5";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "WellStreamfactor";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[12, 66], myExcelWorksheet.Cells[12, 66]);
            Arecord.EthaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "EthaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[14, 66], myExcelWorksheet.Cells[14, 66]);
            Arecord.PropaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "PropaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[15, 66], myExcelWorksheet.Cells[15, 66]);
            Arecord.IsobutaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "IsobutaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[16, 66], myExcelWorksheet.Cells[16, 66]);
            Arecord.NbutaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "NbutaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[18, 66], myExcelWorksheet.Cells[18, 66]);
            Arecord.IsopentaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "IsopentaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[19, 66], myExcelWorksheet.Cells[19, 66]);
            Arecord.NpentaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "NpentaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[205, 66], myExcelWorksheet.Cells[205, 66]);
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
            }
            else
            {
                xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[27, 66], myExcelWorksheet.Cells[27, 66]);
            }
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[27, 66], myExcelWorksheet.Cells[27, 66]);
            Arecord.HexaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "HexaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[45, 66], myExcelWorksheet.Cells[45, 66]);
            Arecord.HeptaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "HeptaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            Arecord.BTUSat = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 36], myExcelWorksheet.Cells[239, 36]);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "BTUSat";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            Arecord.BTUSatAsDelivered = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "BTUSatAsDelivered";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            Arecord.AnalysisDate = DateTime.Now.ToString() + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "AnalysisDate";
            AEntry.value = DateTime.Now.ToString() + "\t";
            Alist.Add(AEntry);
            Arecord.CylinderNumber = txtCylinderNumber.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "CylinderNumber";
            AEntry.value = txtCylinderNumber.Text + "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "RecordStatus";
            AEntry.value = "A\t";
            Alist.Add(AEntry);
            if (rdoGauge.Checked.CompareTo(true) == 0)
            {
                Arecord.SamplePressureGuage = "G\t";
                AEntry = new Flow_Cal_Analysis_Entry();
                AEntry.name = "SamplePressureGuage";
                AEntry.value = "G\t";
                Alist.Add(AEntry);
            }
            else
            {
                Arecord.SamplePressureGuage = "A\t";
                AEntry = new Flow_Cal_Analysis_Entry();
                AEntry.name = "SamplePressureGuage";
                AEntry.value = "A\t";
                Alist.Add(AEntry);
            }
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[242, 9], myExcelWorksheet.Cells[242, 9]);
            Arecord.AtmosphericPressure = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "AtmosphericPressure";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[68, 66], myExcelWorksheet.Cells[68, 66]);
            Arecord.OctaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "OctaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[72, 66], myExcelWorksheet.Cells[72, 66]);
            Arecord.NonaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "NonaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[73, 66], myExcelWorksheet.Cells[73, 66]);
            Arecord.DecaneGPM = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "DecaneGPM";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "twentysixpsigasoline";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "totalmolpercent";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 8], myExcelWorksheet.Cells[239, 8]);
            Arecord.UnNormalizedTotal = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "UnNormalizedTotal";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 23], myExcelWorksheet.Cells[239, 23]);
            Arecord.BTUperLBMass = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "BTUperLBMass";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "NetWobbeIndex";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 88], myExcelWorksheet.Cells[239, 88]);
            Arecord.GrossWobbeIndex = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "GrossWobbeIndex";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "HCDewDewpoint";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "FieldHDCP";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "Cricondentherm";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 22], myExcelWorksheet.Cells[239, 22]);
            Arecord.GrossBTUperCuFt = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "GrossBTUperCuFt";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 34], myExcelWorksheet.Cells[239, 34]);
            Arecord.GrossBTUSatperCuFt = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "GrossBTUSatperCuFt";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 22], myExcelWorksheet.Cells[239, 22]);
            Arecord.GrossBTUperCuFtAsDelivered = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "GrossBTUperCuFtAsDelivered";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 30], myExcelWorksheet.Cells[239, 30]);
            Arecord.SPG = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SPG";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 56], myExcelWorksheet.Cells[239, 56]);
            Arecord.NetBTUrealperCuFt = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "NetBTUrealperCuFt";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "NetBTUSatrealperCuFt";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "NetBTUrealAsDelivered";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[256, 9], myExcelWorksheet.Cells[256, 9]);
            Arecord.TemperatureBase = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "TemperatureBase";
            AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "H2ODewpoint";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 6], myExcelWorksheet.Cells[239, 6]);
            Arecord.TotalMassPercent = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "TotalMassPercent";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "userdefinednumber6";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "HasExtendedAnalysis";            
            if (rdoExtYes.Checked.CompareTo(true) == 0)
            {
                AEntry.value = "Y\t";
            }
            else
            {
                AEntry.value = "N\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "AnalysisType";
            if (rdoGas.Checked.CompareTo(true) == 0)
            {
                Arecord.AnalysisType = "G\t";
                AEntry.value = "G\t";
            }
            else
            {
                Arecord.AnalysisType = "L\t";
                AEntry.value = "L\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[249, 9], myExcelWorksheet.Cells[249, 9]);
            Arecord.CompressibilitySample = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "FreeWater";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "Condensate";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "CompressibilityBase";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "CompressibilitySample";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 12], myExcelWorksheet.Cells[239, 12]);
            Arecord.MolecularWeightSample = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "MolecularWeightSample";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[270, 9], myExcelWorksheet.Cells[270, 9]);
            Arecord.AbsoluteDensity = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "AbsoluteDensity";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 33], myExcelWorksheet.Cells[239, 33]);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[272, 9], myExcelWorksheet.Cells[272, 9]);
            Arecord.RelativeDensityLiq = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "RelativeDensityLiq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 67], myExcelWorksheet.Cells[239, 67]);
            Arecord.APIGravity = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "APIGravity";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 91], myExcelWorksheet.Cells[239, 91]);
            Arecord.GasLiquidVolumeRatio = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "GasLiquidVolumeRatio";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            Arecord.MolWtOrLiqVol = "mole %\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "MolWtOrLiqVol";
            AEntry.value = "mole %\t";
            Alist.Add(AEntry);
            Arecord.SampleID = txtSampleID.Text + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "ProductCode";
            AEntry.value = "ETHYLENE\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SampleID";
            AEntry.value = txtSampleID.Text + "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[74, 10], myExcelWorksheet.Cells[74, 10]);
            Arecord.molpercentEthylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentEthylene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[75, 10], myExcelWorksheet.Cells[75, 10]);
            Arecord.molpercentPropylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentPropylene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[275, 9], myExcelWorksheet.Cells[275, 9]);
            Arecord.molpercentButylenes = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "molpercentButylenes";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "BasedensityCalcMethod";
            AEntry.value = "H\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "DensityByRange";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "AnalysisCertificateNumber";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "AbsoluteDensityLiqAir";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "SampleCalcMethod";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[239, 62], myExcelWorksheet.Cells[239, 62]);
            Arecord.VaporPressure = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "VaporPressure";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                AEntry.value = "\t";
            }
            Alist.Add(AEntry);
            AEntry = new Flow_Cal_Analysis_Entry();
            AEntry.name = "DensityOffset";
            AEntry.value = "\t";
            Alist.Add(AEntry);
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "EVP100";
            //AEntry.value = "\t";
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[11, 14], myExcelWorksheet.Cells[11, 14]);
            //Arecord.WtpercentMethane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentMethane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[12, 14], myExcelWorksheet.Cells[12, 14]);
            //Arecord.WtpercentEthane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentEthane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[14, 14], myExcelWorksheet.Cells[14, 14]);
            //Arecord.WtpercentPropane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentPropane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[15, 14], myExcelWorksheet.Cells[15, 14]);
            //Arecord.WtpercentIsobutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentIsobutane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[16, 14], myExcelWorksheet.Cells[16, 14]);
            //Arecord.WtpercentNbutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentNbutane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[18, 14], myExcelWorksheet.Cells[18, 14]);
            //Arecord.WtpercentIsopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentIsopentane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[19, 14], myExcelWorksheet.Cells[19, 14]);
            //Arecord.WtpercentNpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentNpentane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[27, 14], myExcelWorksheet.Cells[27, 14]);
            //Arecord.WtpercentHexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentHexane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[45, 14], myExcelWorksheet.Cells[45, 14]);
            //Arecord.WtpercentHeptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentHeptane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[68, 14], myExcelWorksheet.Cells[68, 14]);
            //Arecord.WtpercentOctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentOctane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[72, 14], myExcelWorksheet.Cells[72, 14]);
            //Arecord.WtpercentNonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentNonane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[73, 14], myExcelWorksheet.Cells[73, 14]);
            //Arecord.WtpercentDecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentDecane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[3, 14], myExcelWorksheet.Cells[3, 14]);
            //Arecord.WtpercentCO2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentCO2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[8, 14], myExcelWorksheet.Cells[8, 14]);
            //Arecord.WtpercentN2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentN2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[2, 14], myExcelWorksheet.Cells[2, 14]);
            //Arecord.WtpercentCO = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentCO";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[6, 14], myExcelWorksheet.Cells[6, 14]);
            //Arecord.WtpercentO2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentO2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[10, 14], myExcelWorksheet.Cells[10, 14]);
            //Arecord.WtpercentHe = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentHe";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[7, 14], myExcelWorksheet.Cells[7, 14]);
            //Arecord.WtpercentAr = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentAr";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[9, 14], myExcelWorksheet.Cells[9, 14]);
            //Arecord.WtpercentH2O = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentH2O";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[13, 14], myExcelWorksheet.Cells[13, 14]);
            //Arecord.WtpercentH2S = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentH2S";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[5, 14], myExcelWorksheet.Cells[5, 14]);
            //Arecord.WtpercentH2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentH2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[74, 14], myExcelWorksheet.Cells[74, 14]);
            //Arecord.WtpercentEthylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentEthylene";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[75, 14], myExcelWorksheet.Cells[75, 14]);
            //Arecord.WtpercentPropylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentPropylene";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[76, 14], myExcelWorksheet.Cells[76, 14]);
            //Arecord.WtpercentButylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentButylene";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[11, 18], myExcelWorksheet.Cells[11, 18]);
            //Arecord.LVpercentMethane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentMethane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[12, 18], myExcelWorksheet.Cells[12, 18]);
            //Arecord.LVpercentEthane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentEthane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[14, 18], myExcelWorksheet.Cells[14, 18]);
            //Arecord.LVpercentPropane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentPropane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[15, 18], myExcelWorksheet.Cells[15, 18]);
            //Arecord.LVpercentIsobutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentIsobutane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[16, 18], myExcelWorksheet.Cells[16, 18]);
            //Arecord.LVpercentNbutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentNbutane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[18, 18], myExcelWorksheet.Cells[18, 18]);
            //Arecord.LVpercentIsopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentIsopentane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[19, 18], myExcelWorksheet.Cells[19, 18]);
            //Arecord.LVpercentNpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentNpentane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[27, 18], myExcelWorksheet.Cells[27, 18]);
            //Arecord.LVpercentHexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentHexane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[45, 18], myExcelWorksheet.Cells[45, 18]);
            //Arecord.LVpercentHeptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentHeptane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[68, 18], myExcelWorksheet.Cells[68, 18]);
            //Arecord.LVpercentOctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentOctane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[72, 18], myExcelWorksheet.Cells[72, 18]);
            //Arecord.LVpercentNonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentNonane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[73, 18], myExcelWorksheet.Cells[73, 18]);
            //Arecord.LVpercentDecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentDecane";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[3, 18], myExcelWorksheet.Cells[3, 18]);
            //Arecord.LVpercentCO2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentCO2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[8, 18], myExcelWorksheet.Cells[8, 18]);
            //Arecord.LVpercentN2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentN2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[2, 18], myExcelWorksheet.Cells[2, 18]);
            //Arecord.LVpercentCO = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentCO";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[6, 18], myExcelWorksheet.Cells[6, 18]);
            //Arecord.LVpercentO2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentO2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[10, 18], myExcelWorksheet.Cells[10, 18]);
            //Arecord.LVpercentHe = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentHe";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[7, 18], myExcelWorksheet.Cells[7, 18]);
            //Arecord.LVpercentAr = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentAr";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[9, 18], myExcelWorksheet.Cells[9, 18]);
            //Arecord.LVpercentH2O = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentH2O";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[13, 18], myExcelWorksheet.Cells[13, 18]);
            //Arecord.LVpercentH2S = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentH2S";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[5, 18], myExcelWorksheet.Cells[5, 18]);
            //Arecord.LVpercentH2 = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentH2";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[74, 18], myExcelWorksheet.Cells[74, 18]);
            //Arecord.LVpercentEthylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentEthylene";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[75, 18], myExcelWorksheet.Cells[75, 18]);
            //Arecord.LVpercentPropylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentPropylene";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[76, 18], myExcelWorksheet.Cells[76, 18]);
            //Arecord.LVpercentButylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentButylene";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[276, 9], myExcelWorksheet.Cells[276, 9]);
            //Arecord.LVpercentButylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "molpercentAromatics";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[277, 9], myExcelWorksheet.Cells[277, 9]);
            //Arecord.LVpercentButylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "WtpercentAromatics";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            //xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[278, 9], myExcelWorksheet.Cells[278, 9]);
            //Arecord.LVpercentButylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //AEntry = new Flow_Cal_Analysis_Entry();
            //AEntry.name = "LVpercentAromatics";
            //if (Convert.ToSingle(xlrange.Value2) > 0)
            //{
            //    AEntry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            //}
            //else
            //{
            //    AEntry.value = "\t";
            //}
            //Alist.Add(AEntry);
            return Alist;
        }

        private string Build_Astring(List<Flow_Cal_Analysis_Entry> Alist)
        {
            string Astring;
            Astring = "";
            for (int i = 0; i < Alist.Count; i++)
            {
                Astring += Alist[i].value;
            }
            return Astring;
        }

        private List<Flow_Cal_Extended_Entry> Fill_Extended_Record()
        {
            Microsoft.Office.Interop.Excel.Application myExcelApp;
            Workbooks myExcelWorkbooks;
            Workbook myExcelWorkBook = null;
            _Worksheet myExcelWorksheet = null;
            Microsoft.Office.Interop.Excel.Range xlrange;
            List<Flow_Cal_Extended_Entry> Xlist = new List<Flow_Cal_Extended_Entry>();
            Flow_Cal_Extended_Entry Xentry = new Flow_Cal_Extended_Entry();
            Flow_Cal_Extended_Record Xrecord;
            object misValue = System.Reflection.Missing.Value; //Missing value object for the excel routines
            Xrecord = new Flow_Cal_Extended_Record();
            myExcelApp = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
            myExcelWorkbooks = myExcelApp.Workbooks;
            myExcelWorkBook = myExcelApp.Workbooks.get_Item(1);
            myExcelWorksheet = (_Worksheet)myExcelWorkBook.Worksheets.get_Item("CALCULATIONS");
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "SourceAnalysisNumber";
            Xentry.value = txtSourceAnalysisNumber.Text;
            Xlist.Add(Xentry);
            Xrecord.SourceAnalysisNumber = txtSourceAnalysisNumber.Text;
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "ColumnType";
            Xentry.value = "X\t";
            Xlist.Add(Xentry);
            Xrecord.ColumnType = "X\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Date";
            Xentry.value = dtpFromdate.Value + "\t";
            Xlist.Add(Xentry);
            Xrecord.Date = dtpFromdate.Text + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Unused";
            Xentry.value = "\t";
            Xlist.Add(Xentry);
            Xrecord.Unused = "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[9, 10], myExcelWorksheet.Cells[9, 10]);
            Xrecord.Water = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Water";
            if (Convert.ToSingle(xlrange.Value2)> 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[2, 10], myExcelWorksheet.Cells[2, 10]);
            Xrecord.CarbonMonoxide = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "CarbonMonoxide";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Argon = "0\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Argon";
            Xentry.value = "\t";
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[6, 10], myExcelWorksheet.Cells[6, 10]);
            Xrecord.Oxygen = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Oxygen";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[10, 10], myExcelWorksheet.Cells[10, 10]);
            Xrecord.Helium = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Helium";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[5, 10], myExcelWorksheet.Cells[5, 10]);
            Xrecord.Hydrogen = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Hydrogen";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[13, 10], myExcelWorksheet.Cells[13, 10]);
            Xrecord.HydrogenSulfide = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "HydrogenSulfide";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[8, 10], myExcelWorksheet.Cells[8, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Nitrogen";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Nitrogen = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[11, 10], myExcelWorksheet.Cells[11, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Methane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Methane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[3, 10], myExcelWorksheet.Cells[3, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "CarbonDioxide";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.CarbonDioxide = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[12, 10], myExcelWorksheet.Cells[12, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Ethane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Ethane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[14, 10], myExcelWorksheet.Cells[14, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Propane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Propane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[15, 10], myExcelWorksheet.Cells[15, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "isoButane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.isoButane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[16, 10], myExcelWorksheet.Cells[16, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nButane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nButane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[17, 10], myExcelWorksheet.Cells[17, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "neoPentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.neoPentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[18, 10], myExcelWorksheet.Cells[18, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "isoPentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.isoPentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[19, 10], myExcelWorksheet.Cells[19, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nPentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nPentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[21, 10], myExcelWorksheet.Cells[21, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two2Dimethylbutane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two2Dimethylbutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[24, 10], myExcelWorksheet.Cells[24, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two3Dimethylbutane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two3Dimethylbutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[26, 10], myExcelWorksheet.Cells[26, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Cyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Cyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[23, 10], myExcelWorksheet.Cells[23, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "twoMethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.twoMethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[25, 10], myExcelWorksheet.Cells[25, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeMethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.threeMethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[27, 10], myExcelWorksheet.Cells[27, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nHexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nHexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[29, 10], myExcelWorksheet.Cells[29, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two2Dimethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two2Dimethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[28, 10], myExcelWorksheet.Cells[28, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Methylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Methylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[30, 10], myExcelWorksheet.Cells[30, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two4Dimethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two4Dimethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[33, 10], myExcelWorksheet.Cells[33, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two23Trimethylbutane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two23Trimethylbutane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[34, 10], myExcelWorksheet.Cells[34, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Benzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Benzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[35, 10], myExcelWorksheet.Cells[35, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three3Dimethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.three3Dimethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[36, 10], myExcelWorksheet.Cells[36, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Cyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Cyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[37, 10], myExcelWorksheet.Cells[37, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "twoMethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.twoMethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[38, 10], myExcelWorksheet.Cells[38, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two3Dimethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two3Dimethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[40, 10], myExcelWorksheet.Cells[40, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one1Dimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one1Dimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[39, 10], myExcelWorksheet.Cells[39, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeMethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.threeMethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[41, 10], myExcelWorksheet.Cells[41, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet3Dimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onet3Dimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[42, 10], myExcelWorksheet.Cells[42, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec3Dimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec3Dimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[44, 10], myExcelWorksheet.Cells[44, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeEthylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.threeEthylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[43, 10], myExcelWorksheet.Cells[43, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet2Dimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onet2Dimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[71, 10], myExcelWorksheet.Cells[71, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two24Trimethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two24Trimethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[45, 10], myExcelWorksheet.Cells[45, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nHeptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nHeptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[47, 10], myExcelWorksheet.Cells[47, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Methylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Methylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[49, 10], myExcelWorksheet.Cells[49, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one13Trimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one13Trimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[48, 10], myExcelWorksheet.Cells[48, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two2Dimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two2Dimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[50, 10], myExcelWorksheet.Cells[50, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2Dimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec2Dimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[53, 10], myExcelWorksheet.Cells[53, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two5Dimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two5Dimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[52, 10], myExcelWorksheet.Cells[52, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two4Dimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two4Dimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[51, 10], myExcelWorksheet.Cells[51, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Ethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Ethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[54, 10], myExcelWorksheet.Cells[54, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two23Trimethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two23Trimethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[55, 10], myExcelWorksheet.Cells[55, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet2c4Trimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onet2c4Trimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[56, 10], myExcelWorksheet.Cells[56, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three3Dimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.three3Dimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[57, 10], myExcelWorksheet.Cells[57, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet2c3Trimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onet2c3Trimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[58, 10], myExcelWorksheet.Cells[58, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two34Trimethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two34Trimethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[60, 10], myExcelWorksheet.Cells[60, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two3Dimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two3Dimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[59, 10], myExcelWorksheet.Cells[59, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Toluene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Toluene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[92, 10], myExcelWorksheet.Cells[92, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one12Trimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one12Trimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[93, 10], myExcelWorksheet.Cells[93, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2t4Trimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec2t4Trimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[61, 10], myExcelWorksheet.Cells[61, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "twoMethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.twoMethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[62, 10], myExcelWorksheet.Cells[62, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "fourMethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.fourMethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[95, 10], myExcelWorksheet.Cells[95, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three4Dimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.three4Dimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[94, 10], myExcelWorksheet.Cells[94, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeMethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.threeMethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[96, 10], myExcelWorksheet.Cells[96, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeEthylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.threeEthylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[97, 10], myExcelWorksheet.Cells[97, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "ct12Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.ct12Dimethylcyclohexane = "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[63, 10], myExcelWorksheet.Cells[63, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec3Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec3Dimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[97, 10], myExcelWorksheet.Cells[97, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2t3Trimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec2t3Trimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[64, 10], myExcelWorksheet.Cells[64, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet4Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onet4Dimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[98, 10], myExcelWorksheet.Cells[98, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two25Trimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two25Trimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[65, 10], myExcelWorksheet.Cells[65, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one1Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one1Dimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[99, 10], myExcelWorksheet.Cells[99, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethylt3ethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethylt3ethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[66, 10], myExcelWorksheet.Cells[66, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethylc3ethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethylc3ethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[67, 10], myExcelWorksheet.Cells[67, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethylt2ethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethylt2ethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[100, 10], myExcelWorksheet.Cells[100, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two24Trimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two24Trimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[101, 10], myExcelWorksheet.Cells[101, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl1ethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl1ethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[102, 10], myExcelWorksheet.Cells[102, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "MethylEthylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Cycloheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xrecord.MethylEthylcyclopentane = "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nOctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[68, 10], myExcelWorksheet.Cells[68, 10]);
            Xrecord.nOctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet2Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onet2Dimethylcyclohexane = "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[103, 10], myExcelWorksheet.Cells[103, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two244Tetramethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two244Tetramethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[104, 10], myExcelWorksheet.Cells[104, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet3Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onet3Dimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[105, 10], myExcelWorksheet.Cells[105, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec4Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec4Dimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[106, 10], myExcelWorksheet.Cells[106, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2c3Trimethylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec2c3Trimethylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[107, 10], myExcelWorksheet.Cells[107, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two44Trimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two44Trimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[108, 10], myExcelWorksheet.Cells[108, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "isopropylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.isopropylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[109, 10], myExcelWorksheet.Cells[109, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two35Trimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two35Trimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[110, 10], myExcelWorksheet.Cells[110, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two2Dimethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two2Dimethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[111, 10], myExcelWorksheet.Cells[111, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two4Dimethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two4Dimethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[112, 10], myExcelWorksheet.Cells[112, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneEthylc2methylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneEthylc2methylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[113, 10], myExcelWorksheet.Cells[113, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two23Trimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two23Trimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[114, 10], myExcelWorksheet.Cells[114, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2Dimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec2Dimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[116, 10], myExcelWorksheet.Cells[116, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two6Dimethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two6Dimethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[117, 10], myExcelWorksheet.Cells[117, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nPropylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nPropylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[118, 10], myExcelWorksheet.Cells[118, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec3c5Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec3c5Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[119, 10], myExcelWorksheet.Cells[119, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Ethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Ethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[120, 10], myExcelWorksheet.Cells[120, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two5Dimethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two5Dimethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[121, 10], myExcelWorksheet.Cells[121, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three5Dimethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.three5Dimethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xrecord.ct13Dimethylcyclohexane = "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[122, 10], myExcelWorksheet.Cells[122, 10]);
            Xrecord.one13Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one13Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[123, 10], myExcelWorksheet.Cells[123, 10]);
            Xrecord.two33Trimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two33Trimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[124, 10], myExcelWorksheet.Cells[124, 10]);
            Xrecord.three3Dimethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three3Dimethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[125, 10], myExcelWorksheet.Cells[125, 10]);
            Xrecord.one14Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one14Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[126, 10], myExcelWorksheet.Cells[126, 10]);
            Xrecord.two233Tetramethylpentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two233Tetramethylpentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[127, 10], myExcelWorksheet.Cells[127, 10]);
            Xrecord.Ethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Ethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[128, 10], myExcelWorksheet.Cells[128, 10]);
            Xrecord.two34Trimethylhexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two34Trimethylhexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[129, 10], myExcelWorksheet.Cells[129, 10]);
            Xrecord.onet2t4Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet2t4Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[130, 10], myExcelWorksheet.Cells[130, 10]);
            Xrecord.onec3t5Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec3t5Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[132, 10], myExcelWorksheet.Cells[132, 10]);
            Xrecord.mXylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "mXylene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[133, 10], myExcelWorksheet.Cells[133, 10]);
            Xrecord.pXylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "pXylene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[135, 10], myExcelWorksheet.Cells[135, 10]);
            Xrecord.three4Dimethylheptane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three4Dimethylheptane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[136, 10], myExcelWorksheet.Cells[136, 10]);
            Xrecord.twoMethyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "twoMethyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[137, 10], myExcelWorksheet.Cells[137, 10]);
            Xrecord.fourMethyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "fourMethyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[138, 10], myExcelWorksheet.Cells[138, 10]);
            Xrecord.threeMethyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeMethyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[139, 10], myExcelWorksheet.Cells[139, 10]);
            Xrecord.onet2c3Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet2c3Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[140, 10], myExcelWorksheet.Cells[140, 10]);
            Xrecord.onet2c4Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onet2c4Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[131, 10], myExcelWorksheet.Cells[131, 10]);
            Xrecord.oXylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oXylene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[141, 10], myExcelWorksheet.Cells[141, 10]);
            Xrecord.one12Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one12Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[184, 10], myExcelWorksheet.Cells[184, 10]);
            Xrecord.onec2t4Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2t4Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[185, 10], myExcelWorksheet.Cells[185, 10]);
            Xrecord.onec2c4Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2c4Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[186, 10], myExcelWorksheet.Cells[186, 10]);
            Xrecord.Isobutylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Isobutylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[72, 10], myExcelWorksheet.Cells[72, 10]);
            Xrecord.nNonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nNonane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two55Trimethylheptane";
            Xentry.value = "\t";
            Xlist.Add(Xentry);
            Xrecord.two55Trimethylheptane = "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[142, 10], myExcelWorksheet.Cells[142, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2t3Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec2t3Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[143, 10], myExcelWorksheet.Cells[143, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onec2c3Trimethylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onec2c3Trimethylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[144, 10], myExcelWorksheet.Cells[144, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Isopropylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Isopropylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[187, 10], myExcelWorksheet.Cells[187, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two2Dimethyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two2Dimethyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[145, 10], myExcelWorksheet.Cells[145, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Isopropylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Isopropylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[146, 10], myExcelWorksheet.Cells[146, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Cyclooctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Cyclooctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[147, 10], myExcelWorksheet.Cells[147, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nButylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nButylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[148, 10], myExcelWorksheet.Cells[148, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nPropylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nPropylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[188, 10], myExcelWorksheet.Cells[188, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three3Dimethyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.three3Dimethyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[149, 10], myExcelWorksheet.Cells[149, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nPropylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nPropylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xrecord.oneMethyl3Ethylbenzene = "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl4Ethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl4Ethylbenzene = "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[189, 10], myExcelWorksheet.Cells[189, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "three6Dimethyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xlist.Add(Xentry);
            Xrecord.three6Dimethyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[150, 10], myExcelWorksheet.Cells[150, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "mEthyltoluene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.mEthyltoluene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[151, 10], myExcelWorksheet.Cells[151, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "pEthyltoluene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.pEthyltoluene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[190, 10], myExcelWorksheet.Cells[190, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "two3Dimethyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.two3Dimethyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[152, 10], myExcelWorksheet.Cells[152, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "fourMethylnonane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.fourMethylnonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[153, 10], myExcelWorksheet.Cells[153, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "fiveMethylnonane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.fiveMethylnonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[154, 10], myExcelWorksheet.Cells[154, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onethree5Trimethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onethree5Trimethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[155, 10], myExcelWorksheet.Cells[155, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "twoMethylnonane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.twoMethylnonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[156, 10], myExcelWorksheet.Cells[156, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeEthyloctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.threeEthyloctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[157, 10], myExcelWorksheet.Cells[157, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "threeMethylnonane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.threeMethylnonane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[158, 10], myExcelWorksheet.Cells[158, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oEthyltoluene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oEthyltoluene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[159, 10], myExcelWorksheet.Cells[159, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one24Trimethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one24Trimethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[160, 10], myExcelWorksheet.Cells[160, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "tertButylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.tertButylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[161, 10], myExcelWorksheet.Cells[161, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Methylcyclooctane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Methylcyclooctane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[162, 10], myExcelWorksheet.Cells[162, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "tertButylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.tertButylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[163, 10], myExcelWorksheet.Cells[163, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Isobutylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Isobutylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[73, 10], myExcelWorksheet.Cells[73, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nDecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nDecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[164, 10], myExcelWorksheet.Cells[164, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Isobutylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Isobutylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[165, 10], myExcelWorksheet.Cells[165, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "secButylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.secButylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[166, 10], myExcelWorksheet.Cells[166, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl3isopropylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl3isopropylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[167, 10], myExcelWorksheet.Cells[167, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one23Trimethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one23Trimethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[168, 10], myExcelWorksheet.Cells[168, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl4isopropylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl4isopropylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[169, 10], myExcelWorksheet.Cells[169, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl2isopropylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl2isopropylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[170, 10], myExcelWorksheet.Cells[170, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nButylcyclohexane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nButylcyclohexane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[171, 10], myExcelWorksheet.Cells[171, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one3Diethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one3Diethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[172, 10], myExcelWorksheet.Cells[172, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl3propylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl3propylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[173, 10], myExcelWorksheet.Cells[173, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one2Diethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one2Diethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[174, 10], myExcelWorksheet.Cells[174, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nButylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nButylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[175, 10], myExcelWorksheet.Cells[175, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl4propylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl4propylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[176, 10], myExcelWorksheet.Cells[176, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one4Diethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one4Diethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[177, 10], myExcelWorksheet.Cells[177, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethyl2propylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethyl2propylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[191, 10], myExcelWorksheet.Cells[191, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one4Dimethyl2ethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one4Dimethyl2ethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[192, 10], myExcelWorksheet.Cells[192, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one2Dimethyl4ethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one2Dimethyl4ethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[193, 10], myExcelWorksheet.Cells[193, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one3Dimethyl2ethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one3Dimethyl2ethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[194, 10], myExcelWorksheet.Cells[194, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one2Dimethyl3ethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one2Dimethyl3ethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[217, 10], myExcelWorksheet.Cells[217, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nUndecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nUndecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[195, 10], myExcelWorksheet.Cells[195, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "twoMethylbutylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.twoMethylbutylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[178, 10], myExcelWorksheet.Cells[178, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one245Tetramethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one245Tetramethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[179, 10], myExcelWorksheet.Cells[179, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one2three5Tetramethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one2three5Tetramethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[196, 10], myExcelWorksheet.Cells[196, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onetertButyl2methylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onetertButyl2methylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[180, 10], myExcelWorksheet.Cells[180, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one2three4Tetramethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one2three4Tetramethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[181, 10], myExcelWorksheet.Cells[181, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Cyclodecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Cyclodecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[197, 10], myExcelWorksheet.Cells[197, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nPentylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nPentylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[182, 10], myExcelWorksheet.Cells[182, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Naphthalene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Naphthalene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[218, 10], myExcelWorksheet.Cells[218, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nDodecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nDodecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[198, 10], myExcelWorksheet.Cells[198, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "oneMethylt24methylpentylcyclopentane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.oneMethylt24methylpentylcyclopentane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[199, 10], myExcelWorksheet.Cells[199, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onetertButylthree5dimethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onetertButylthree5dimethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[200, 10], myExcelWorksheet.Cells[200, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "one24Triethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.one24Triethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[201, 10], myExcelWorksheet.Cells[201, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "onethree5Triethylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.onethree5Triethylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[202, 10], myExcelWorksheet.Cells[202, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nHexylbenzene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nHexylbenzene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[219, 10], myExcelWorksheet.Cells[219, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nTridecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nTridecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[220, 10], myExcelWorksheet.Cells[220, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nTetradecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nTetradecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[221, 10], myExcelWorksheet.Cells[221, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "nPentadecane";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.nPentadecane = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[74, 10], myExcelWorksheet.Cells[74, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Ethylene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Ethylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[75, 10], myExcelWorksheet.Cells[75, 10]);
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Propylene";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Xentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Xentry.value = "\t";
            }
            Xlist.Add(Xentry);
            Xrecord.Propylene = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            Xentry = new Flow_Cal_Extended_Entry();
            Xentry.name = "Butylenes";
            Xentry.value = "\t";
            Xlist.Add(Xentry);
            Xrecord.Butylenes = "\t";
            return Xlist;
        }

        private string Build_Xstring(List<Flow_Cal_Extended_Entry> Xlist)
        {
            string Xstring;
            Xstring = "";
            for (int i = 0; i < Xlist.Count; i++)
            {
                Xstring += Xlist[i].value;
            }
            return Xstring;
        }

        private List<Flow_Cal_Extended_Chars_Entry> Fill_Extended_Chars_Record()
        {
            Microsoft.Office.Interop.Excel.Application myExcelApp;
            Workbooks myExcelWorkbooks;
            Workbook myExcelWorkBook = null;
            _Worksheet myExcelWorksheet = null;
            Microsoft.Office.Interop.Excel.Range xlrange;
            List<Flow_Cal_Extended_Chars_Entry> Wlist = new List<Flow_Cal_Extended_Chars_Entry>();
            Flow_Cal_Extended_Chars_Entry Wentry = new Flow_Cal_Extended_Chars_Entry();
            object misValue = System.Reflection.Missing.Value; //Missing value object for the excel routines
            myExcelApp = (Microsoft.Office.Interop.Excel.Application)System.Runtime.InteropServices.Marshal.GetActiveObject("Excel.Application");
            myExcelWorkbooks = myExcelApp.Workbooks;
            myExcelWorkBook = myExcelApp.Workbooks.get_Item(1);
            myExcelWorksheet = (_Worksheet)myExcelWorkBook.Worksheets.get_Item("Carbon Groups");
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            Wentry.name = "SourceAnalysisNumber";
            Wentry.value = txtSourceAnalysisNumber.Text;
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            Wentry.name = "ColumnType";
            Wentry.value = "W\t";
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            Wentry.name = "SampleDate";
            Wentry.value = dtpSampleDateTime.Value + "\t";
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[6, 2], myExcelWorksheet.Cells[6, 2]);
            Wentry.name = "c6plus_molar_mass";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[7, 2], myExcelWorksheet.Cells[7, 2]);
            Wentry.name = "c6plus_relative_density_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[8, 2], myExcelWorksheet.Cells[8, 2]);
            Wentry.name = "c6plus_absolute_density_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[9, 2], myExcelWorksheet.Cells[9, 2]);
            Wentry.name = "c6plus_gl_ratio";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[10, 2], myExcelWorksheet.Cells[10, 2]);
            Wentry.name = "c6plus_gross_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[11, 2], myExcelWorksheet.Cells[11, 2]);
            Wentry.name = "c6plus_gross_hv_gas_per_gal";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[12, 2], myExcelWorksheet.Cells[12, 2]);
            Wentry.name = "c6plus_mass_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[13, 2], myExcelWorksheet.Cells[13, 2]);
            Wentry.name = "c6plus_gross_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[14, 2], myExcelWorksheet.Cells[14, 2]);
            Wentry.name = "c6plus_mass_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[15, 2], myExcelWorksheet.Cells[15, 2]);
            Wentry.name = "c6plus_net_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[16, 2], myExcelWorksheet.Cells[16, 2]);
            Wentry.name = "c6plus_vapor_pressure";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[17, 2], myExcelWorksheet.Cells[17, 2]);
            Wentry.name = "c6plus_heat_of_vaporization";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[18, 2], myExcelWorksheet.Cells[18, 2]);
            Wentry.name = "c6plus_relative_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[19, 2], myExcelWorksheet.Cells[19, 2]);
            Wentry.name = "c7plus_molar_mass";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[20, 2], myExcelWorksheet.Cells[20, 2]);
            Wentry.name = "c7plus_relative_density_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[21, 2], myExcelWorksheet.Cells[21, 2]);
            Wentry.name = "c7plus_absolute_density_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[22, 2], myExcelWorksheet.Cells[22, 2]);
            Wentry.name = "c7plus_gl_ratio";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[23, 2], myExcelWorksheet.Cells[23, 2]);
            Wentry.name = "c7plus_gross_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[24, 2], myExcelWorksheet.Cells[24, 2]);
            Wentry.name = "c7plus_gross_hv_gas_per_gal";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[25, 2], myExcelWorksheet.Cells[25, 2]);
            Wentry.name = "c7plus_mass_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[26, 2], myExcelWorksheet.Cells[26, 2]);
            Wentry.name = "c7plus_gross_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[27, 2], myExcelWorksheet.Cells[27, 2]);
            Wentry.name = "c7plus_mass_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[28, 2], myExcelWorksheet.Cells[28, 2]);
            Wentry.name = "c7plus_net_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[29, 2], myExcelWorksheet.Cells[29, 2]);
            Wentry.name = "c7plus_vapor_pressure";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[30, 2], myExcelWorksheet.Cells[30, 2]);
            Wentry.name = "c7plus_heat_of_vaporization";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[31, 2], myExcelWorksheet.Cells[31, 2]);
            Wentry.name = "c7plus_relative_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[32, 2], myExcelWorksheet.Cells[32, 2]);
            Wentry.name = "c8plus_molar_mass";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[33, 2], myExcelWorksheet.Cells[33, 2]);
            Wentry.name = "c8plus_relative_density_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[34, 2], myExcelWorksheet.Cells[34, 2]);
            Wentry.name = "c8plus_absolute_density_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[35, 2], myExcelWorksheet.Cells[35, 2]);
            Wentry.name = "c8plus_gl_ratio";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[36, 2], myExcelWorksheet.Cells[36, 2]);
            Wentry.name = "c8plus_gross_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[37, 2], myExcelWorksheet.Cells[37, 2]);
            Wentry.name = "c8plus_gross_hv_gas_per_gal";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[38, 2], myExcelWorksheet.Cells[38, 2]);
            Wentry.name = "c8plus_mass_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[39, 2], myExcelWorksheet.Cells[39, 2]);
            Wentry.name = "c8plus_gross_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[40, 2], myExcelWorksheet.Cells[40, 2]);
            Wentry.name = "c8plus_mass_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[41, 2], myExcelWorksheet.Cells[41, 2]);
            Wentry.name = "c8plus_net_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[42, 2], myExcelWorksheet.Cells[42, 2]);
            Wentry.name = "c8plus_vapor_pressure";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[43, 2], myExcelWorksheet.Cells[43, 2]);
            Wentry.name = "c8plus_heat_of_vaporization";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[44, 2], myExcelWorksheet.Cells[44, 2]);
            Wentry.name = "c8plus_relative_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[45, 2], myExcelWorksheet.Cells[45, 2]);
            Wentry.name = "c9plus_molar_mass";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[46, 2], myExcelWorksheet.Cells[46, 2]);
            Wentry.name = "c9plus_relative_density_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[47, 2], myExcelWorksheet.Cells[47, 2]);
            Wentry.name = "c9plus_absolute_density_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[48, 2], myExcelWorksheet.Cells[48, 2]);
            Wentry.name = "c9plus_gl_ratio";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[49, 2], myExcelWorksheet.Cells[49, 2]);
            Wentry.name = "c9plus_gross_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[50, 2], myExcelWorksheet.Cells[50, 2]);
            Wentry.name = "c9plus_gross_hv_gas_per_gal";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[51, 2], myExcelWorksheet.Cells[51, 2]);
            Wentry.name = "c9plus_mass_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[52, 2], myExcelWorksheet.Cells[52, 2]);
            Wentry.name = "c9plus_gross_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[53, 2], myExcelWorksheet.Cells[53, 2]);
            Wentry.name = "c9plus_mass_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[54, 2], myExcelWorksheet.Cells[54, 2]);
            Wentry.name = "c9plus_net_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[55, 2], myExcelWorksheet.Cells[55, 2]);
            Wentry.name = "c9plus_vapor_pressure";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[56, 2], myExcelWorksheet.Cells[56, 2]);
            Wentry.name = "c9plus_heat_of_vaporization";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[57, 2], myExcelWorksheet.Cells[57, 2]);
            Wentry.name = "c9plus_relative_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[58, 2], myExcelWorksheet.Cells[58, 2]);
            Wentry.name = "c10plus_molar_mass";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[59, 2], myExcelWorksheet.Cells[59, 2]);
            Wentry.name = "c10plus_relative_density_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[60, 2], myExcelWorksheet.Cells[60, 2]);
            Wentry.name = "c10plus_absolute_density_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[61, 2], myExcelWorksheet.Cells[61, 2]);
            Wentry.name = "c10plus_gl_ratio";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[62, 2], myExcelWorksheet.Cells[62, 2]);
            Wentry.name = "c10plus_gross_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[63, 2], myExcelWorksheet.Cells[63, 2]);
            Wentry.name = "c10plus_gross_hv_gas_per_gal";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[64, 2], myExcelWorksheet.Cells[64, 2]);
            Wentry.name = "c10plus_mass_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[65, 2], myExcelWorksheet.Cells[65, 2]);
            Wentry.name = "c10plus_gross_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[66, 2], myExcelWorksheet.Cells[66, 2]);
            Wentry.name = "c10plus_mass_hv_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[67, 2], myExcelWorksheet.Cells[67, 2]);
            Wentry.name = "c10plus_net_hv_gas";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[68, 2], myExcelWorksheet.Cells[68, 2]);
            Wentry.name = "c10plus_vapor_pressure";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[69, 2], myExcelWorksheet.Cells[69, 2]);
            Wentry.name = "c10plus_heat_of_vaporization";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[70, 2], myExcelWorksheet.Cells[70, 2]);
            Wentry.name = "c10plus_relative_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[71, 2], myExcelWorksheet.Cells[71, 2]);
            Wentry.name = "c6plus_api_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[72, 2], myExcelWorksheet.Cells[72, 2]);
            Wentry.name = "c7plus_api_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[73, 2], myExcelWorksheet.Cells[73, 2]);
            Wentry.name = "c8plus_api_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[74, 2], myExcelWorksheet.Cells[74, 2]);
            Wentry.name = "c9plus_api_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[75, 2], myExcelWorksheet.Cells[75, 2]);
            Wentry.name = "c10plus_api_dens_liq";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[76, 2], myExcelWorksheet.Cells[76, 2]);
            Wentry.name = "c6plus_abs_dens_liq_air";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[77, 2], myExcelWorksheet.Cells[77, 2]);
            Wentry.name = "c7plus_abs_dens_liq_air";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[78, 2], myExcelWorksheet.Cells[78, 2]);
            Wentry.name = "c8plus_abs_dens_liq_air";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[79, 2], myExcelWorksheet.Cells[79, 2]);
            Wentry.name = "c9plus_abs_dens_liq_air";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            Wentry = new Flow_Cal_Extended_Chars_Entry();
            xlrange = myExcelWorksheet.get_Range(myExcelWorksheet.Cells[80, 2], myExcelWorksheet.Cells[80, 2]);
            Wentry.name = "c10plus_abs_dens_liq_air";
            if (Convert.ToSingle(xlrange.Value2) > 0)
            {
                Wentry.value = String.Format("{0:0.###}", xlrange.Value2) + "\t";
            }
            else
            {
                Wentry.value = "\t";
            }
            Wlist.Add(Wentry);
            return Wlist;
        }
        
        private string Build_Wstring(List<Flow_Cal_Extended_Chars_Entry> Wlist)
        {
            string Wstring;
            Wstring = "";
            for (int i = 0; i < Wlist.Count; i++)
            {
                Wstring += Wlist[i].value;
            }
            return Wstring;
        }

        private void selectOutputFileLocationToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog Flow_Cal_Output_Folder = new FolderBrowserDialog();
            Flow_Cal_Output_Folder.SelectedPath = "C:\\Flow-Cal\\";
            DialogResult result = Flow_Cal_Output_Folder.ShowDialog();
            if (result == DialogResult.OK) // Test result.
            {
                Flow_Cal_Output_Folder1 = Flow_Cal_Output_Folder.SelectedPath;
                lblFlowCalOutputFolder.Text = Flow_Cal_Output_Folder1 + "\\";
            }

        }
 


 



    }
}
