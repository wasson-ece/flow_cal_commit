﻿namespace Flow_Cal_Commit
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.cbSayboltColor = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtSampleTechnician = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.txtAnalysisTechnician = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtSampleNumber = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.txtSampleID = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.txtCylinderNumber = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtSamplePressure = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSampleTemperature = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rdoAbsolute = new System.Windows.Forms.RadioButton();
            this.rdoGauge = new System.Windows.Forms.RadioButton();
            this.txtFieldComments = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtOfficeComments = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.cmdBuildFlowCalFile = new System.Windows.Forms.Button();
            this.dtpFromdate = new System.Windows.Forms.DateTimePicker();
            this.dtpSampleDateTime = new System.Windows.Forms.DateTimePicker();
            this.txtInstrumentNumber = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.grpHasExtendedAnalysis = new System.Windows.Forms.GroupBox();
            this.rdoExtNo = new System.Windows.Forms.RadioButton();
            this.rdoExtYes = new System.Windows.Forms.RadioButton();
            this.grpGasLiq = new System.Windows.Forms.GroupBox();
            this.rdoLiquid = new System.Windows.Forms.RadioButton();
            this.rdoGas = new System.Windows.Forms.RadioButton();
            this.txtSourceAnalysisNumber = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.selectOutputFileLocationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.lblFlowCalOutputFolder = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.grpWRecord = new System.Windows.Forms.GroupBox();
            this.rdoWNo = new System.Windows.Forms.RadioButton();
            this.rdoWYes = new System.Windows.Forms.RadioButton();
            this.cbCopperStrip = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.mseH2S = new System.Windows.Forms.MaskedTextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.grpHasExtendedAnalysis.SuspendLayout();
            this.grpGasLiq.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.grpWRecord.SuspendLayout();
            this.SuspendLayout();
            // 
            // cbSayboltColor
            // 
            this.cbSayboltColor.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbSayboltColor.FormattingEnabled = true;
            this.cbSayboltColor.Items.AddRange(new object[] {
            "None",
            "+30",
            "+29",
            "+28",
            "+27",
            "+26",
            "+25",
            "+24",
            "+23",
            "+22",
            "+21",
            "+20",
            "+19",
            "+18",
            "+17",
            "+16",
            "+15",
            "+14",
            "+13",
            "+12",
            "+11",
            "+10",
            "+9",
            "+8",
            "+7",
            "+6",
            "+5",
            "+4",
            "+3",
            "+2",
            "+1",
            "0",
            "-1",
            "-2",
            "-3",
            "-4",
            "-5",
            "-6",
            "-7",
            "-8",
            "-9",
            "-10",
            "-11",
            "-12",
            "-13",
            "-14",
            "-15",
            "-16"});
            this.cbSayboltColor.Location = new System.Drawing.Point(119, 246);
            this.cbSayboltColor.Name = "cbSayboltColor";
            this.cbSayboltColor.Size = new System.Drawing.Size(181, 21);
            this.cbSayboltColor.TabIndex = 40;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(160, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "From Date/Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(596, 20);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Sample Date/Time";
            // 
            // txtSampleTechnician
            // 
            this.txtSampleTechnician.Location = new System.Drawing.Point(118, 82);
            this.txtSampleTechnician.Name = "txtSampleTechnician";
            this.txtSampleTechnician.Size = new System.Drawing.Size(182, 20);
            this.txtSampleTechnician.TabIndex = 6;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(160, 66);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(98, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Sample Technician";
            // 
            // txtAnalysisTechnician
            // 
            this.txtAnalysisTechnician.Location = new System.Drawing.Point(337, 82);
            this.txtAnalysisTechnician.Name = "txtAnalysisTechnician";
            this.txtAnalysisTechnician.Size = new System.Drawing.Size(182, 20);
            this.txtAnalysisTechnician.TabIndex = 8;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(383, 66);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(101, 13);
            this.label5.TabIndex = 9;
            this.label5.Text = "Analysis Technician";
            // 
            // txtSampleNumber
            // 
            this.txtSampleNumber.Location = new System.Drawing.Point(546, 82);
            this.txtSampleNumber.Name = "txtSampleNumber";
            this.txtSampleNumber.Size = new System.Drawing.Size(191, 20);
            this.txtSampleNumber.TabIndex = 10;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(596, 66);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(82, 13);
            this.label6.TabIndex = 11;
            this.label6.Text = "Sample Number";
            // 
            // txtSampleID
            // 
            this.txtSampleID.Location = new System.Drawing.Point(118, 137);
            this.txtSampleID.Name = "txtSampleID";
            this.txtSampleID.Size = new System.Drawing.Size(182, 20);
            this.txtSampleID.TabIndex = 12;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(175, 118);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(56, 13);
            this.label7.TabIndex = 13;
            this.label7.Text = "Sample ID";
            // 
            // txtCylinderNumber
            // 
            this.txtCylinderNumber.Location = new System.Drawing.Point(337, 138);
            this.txtCylinderNumber.Name = "txtCylinderNumber";
            this.txtCylinderNumber.Size = new System.Drawing.Size(181, 20);
            this.txtCylinderNumber.TabIndex = 14;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(383, 118);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(84, 13);
            this.label8.TabIndex = 15;
            this.label8.Text = "Cylinder Number";
            // 
            // txtSamplePressure
            // 
            this.txtSamplePressure.Location = new System.Drawing.Point(546, 138);
            this.txtSamplePressure.Name = "txtSamplePressure";
            this.txtSamplePressure.Size = new System.Drawing.Size(191, 20);
            this.txtSamplePressure.TabIndex = 16;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(596, 118);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(86, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Sample Pressure";
            // 
            // txtSampleTemperature
            // 
            this.txtSampleTemperature.Location = new System.Drawing.Point(118, 190);
            this.txtSampleTemperature.Name = "txtSampleTemperature";
            this.txtSampleTemperature.Size = new System.Drawing.Size(182, 20);
            this.txtSampleTemperature.TabIndex = 18;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(160, 171);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(105, 13);
            this.label10.TabIndex = 19;
            this.label10.Text = "Sample Temperature";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.rdoAbsolute);
            this.groupBox1.Controls.Add(this.rdoGauge);
            this.groupBox1.Location = new System.Drawing.Point(546, 236);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(191, 47);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Sample Pressure";
            // 
            // rdoAbsolute
            // 
            this.rdoAbsolute.AutoSize = true;
            this.rdoAbsolute.Location = new System.Drawing.Point(100, 19);
            this.rdoAbsolute.Name = "rdoAbsolute";
            this.rdoAbsolute.Size = new System.Drawing.Size(66, 17);
            this.rdoAbsolute.TabIndex = 1;
            this.rdoAbsolute.Text = "Absolute";
            this.rdoAbsolute.UseVisualStyleBackColor = true;
            // 
            // rdoGauge
            // 
            this.rdoGauge.AutoSize = true;
            this.rdoGauge.Checked = true;
            this.rdoGauge.Location = new System.Drawing.Point(18, 19);
            this.rdoGauge.Name = "rdoGauge";
            this.rdoGauge.Size = new System.Drawing.Size(57, 17);
            this.rdoGauge.TabIndex = 0;
            this.rdoGauge.TabStop = true;
            this.rdoGauge.Text = "Guage";
            this.rdoGauge.UseVisualStyleBackColor = true;
            // 
            // txtFieldComments
            // 
            this.txtFieldComments.Location = new System.Drawing.Point(102, 324);
            this.txtFieldComments.MinimumSize = new System.Drawing.Size(800, 100);
            this.txtFieldComments.Multiline = true;
            this.txtFieldComments.Name = "txtFieldComments";
            this.txtFieldComments.Size = new System.Drawing.Size(806, 100);
            this.txtFieldComments.TabIndex = 21;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(99, 308);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(74, 13);
            this.label11.TabIndex = 22;
            this.label11.Text = "Field Remarks";
            // 
            // txtOfficeComments
            // 
            this.txtOfficeComments.Location = new System.Drawing.Point(102, 464);
            this.txtOfficeComments.MinimumSize = new System.Drawing.Size(800, 100);
            this.txtOfficeComments.Multiline = true;
            this.txtOfficeComments.Name = "txtOfficeComments";
            this.txtOfficeComments.Size = new System.Drawing.Size(806, 100);
            this.txtOfficeComments.TabIndex = 23;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(99, 448);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 13);
            this.label12.TabIndex = 24;
            this.label12.Text = "Office Remarks";
            // 
            // cmdBuildFlowCalFile
            // 
            this.cmdBuildFlowCalFile.Location = new System.Drawing.Point(857, 43);
            this.cmdBuildFlowCalFile.Name = "cmdBuildFlowCalFile";
            this.cmdBuildFlowCalFile.Size = new System.Drawing.Size(111, 38);
            this.cmdBuildFlowCalFile.TabIndex = 25;
            this.cmdBuildFlowCalFile.Text = "Build The Flow_Cal File";
            this.cmdBuildFlowCalFile.UseVisualStyleBackColor = true;
            this.cmdBuildFlowCalFile.Click += new System.EventHandler(this.cmdBuildFlowCalFile_Click);
            // 
            // dtpFromdate
            // 
            this.dtpFromdate.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.dtpFromdate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpFromdate.Location = new System.Drawing.Point(118, 36);
            this.dtpFromdate.Name = "dtpFromdate";
            this.dtpFromdate.Size = new System.Drawing.Size(182, 20);
            this.dtpFromdate.TabIndex = 27;
            // 
            // dtpSampleDateTime
            // 
            this.dtpSampleDateTime.CustomFormat = "MM/dd/yyyy hh:mm tt";
            this.dtpSampleDateTime.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpSampleDateTime.Location = new System.Drawing.Point(547, 36);
            this.dtpSampleDateTime.Name = "dtpSampleDateTime";
            this.dtpSampleDateTime.Size = new System.Drawing.Size(190, 20);
            this.dtpSampleDateTime.TabIndex = 29;
            // 
            // txtInstrumentNumber
            // 
            this.txtInstrumentNumber.Location = new System.Drawing.Point(337, 190);
            this.txtInstrumentNumber.Name = "txtInstrumentNumber";
            this.txtInstrumentNumber.Size = new System.Drawing.Size(182, 20);
            this.txtInstrumentNumber.TabIndex = 30;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(383, 171);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(96, 13);
            this.label13.TabIndex = 31;
            this.label13.Text = "Instrument Number";
            // 
            // grpHasExtendedAnalysis
            // 
            this.grpHasExtendedAnalysis.Controls.Add(this.rdoExtNo);
            this.grpHasExtendedAnalysis.Controls.Add(this.rdoExtYes);
            this.grpHasExtendedAnalysis.Location = new System.Drawing.Point(828, 174);
            this.grpHasExtendedAnalysis.Name = "grpHasExtendedAnalysis";
            this.grpHasExtendedAnalysis.Size = new System.Drawing.Size(140, 47);
            this.grpHasExtendedAnalysis.TabIndex = 32;
            this.grpHasExtendedAnalysis.TabStop = false;
            this.grpHasExtendedAnalysis.Text = "Has Extend Analysis";
            // 
            // rdoExtNo
            // 
            this.rdoExtNo.AutoSize = true;
            this.rdoExtNo.Checked = true;
            this.rdoExtNo.Location = new System.Drawing.Point(75, 16);
            this.rdoExtNo.Name = "rdoExtNo";
            this.rdoExtNo.Size = new System.Drawing.Size(39, 17);
            this.rdoExtNo.TabIndex = 1;
            this.rdoExtNo.TabStop = true;
            this.rdoExtNo.Text = "No";
            this.rdoExtNo.UseVisualStyleBackColor = true;
            // 
            // rdoExtYes
            // 
            this.rdoExtYes.AutoSize = true;
            this.rdoExtYes.Location = new System.Drawing.Point(15, 16);
            this.rdoExtYes.Name = "rdoExtYes";
            this.rdoExtYes.Size = new System.Drawing.Size(43, 17);
            this.rdoExtYes.TabIndex = 0;
            this.rdoExtYes.Text = "Yes";
            this.rdoExtYes.UseVisualStyleBackColor = true;
            // 
            // grpGasLiq
            // 
            this.grpGasLiq.Controls.Add(this.rdoLiquid);
            this.grpGasLiq.Controls.Add(this.rdoGas);
            this.grpGasLiq.Location = new System.Drawing.Point(828, 118);
            this.grpGasLiq.Name = "grpGasLiq";
            this.grpGasLiq.Size = new System.Drawing.Size(140, 50);
            this.grpGasLiq.TabIndex = 33;
            this.grpGasLiq.TabStop = false;
            this.grpGasLiq.Text = "Gas/Liquid";
            // 
            // rdoLiquid
            // 
            this.rdoLiquid.AutoSize = true;
            this.rdoLiquid.Checked = true;
            this.rdoLiquid.Location = new System.Drawing.Point(75, 20);
            this.rdoLiquid.Name = "rdoLiquid";
            this.rdoLiquid.Size = new System.Drawing.Size(53, 17);
            this.rdoLiquid.TabIndex = 1;
            this.rdoLiquid.TabStop = true;
            this.rdoLiquid.Text = "Liquid";
            this.rdoLiquid.UseVisualStyleBackColor = true;
            // 
            // rdoGas
            // 
            this.rdoGas.AutoSize = true;
            this.rdoGas.Location = new System.Drawing.Point(15, 20);
            this.rdoGas.Name = "rdoGas";
            this.rdoGas.Size = new System.Drawing.Size(44, 17);
            this.rdoGas.TabIndex = 0;
            this.rdoGas.Text = "Gas";
            this.rdoGas.UseVisualStyleBackColor = true;
            // 
            // txtSourceAnalysisNumber
            // 
            this.txtSourceAnalysisNumber.Location = new System.Drawing.Point(338, 36);
            this.txtSourceAnalysisNumber.Name = "txtSourceAnalysisNumber";
            this.txtSourceAnalysisNumber.Size = new System.Drawing.Size(182, 20);
            this.txtSourceAnalysisNumber.TabIndex = 34;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(383, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(74, 13);
            this.label14.TabIndex = 35;
            this.label14.Text = "Meter Number";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.selectOutputFileLocationToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1010, 24);
            this.menuStrip1.TabIndex = 36;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // selectOutputFileLocationToolStripMenuItem
            // 
            this.selectOutputFileLocationToolStripMenuItem.Name = "selectOutputFileLocationToolStripMenuItem";
            this.selectOutputFileLocationToolStripMenuItem.Size = new System.Drawing.Size(161, 20);
            this.selectOutputFileLocationToolStripMenuItem.Text = "Select Output File Location";
            this.selectOutputFileLocationToolStripMenuItem.Click += new System.EventHandler(this.selectOutputFileLocationToolStripMenuItem_Click);
            // 
            // lblFlowCalOutputFolder
            // 
            this.lblFlowCalOutputFolder.AutoSize = true;
            this.lblFlowCalOutputFolder.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.lblFlowCalOutputFolder.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lblFlowCalOutputFolder.Location = new System.Drawing.Point(313, 586);
            this.lblFlowCalOutputFolder.MinimumSize = new System.Drawing.Size(500, 15);
            this.lblFlowCalOutputFolder.Name = "lblFlowCalOutputFolder";
            this.lblFlowCalOutputFolder.Size = new System.Drawing.Size(500, 15);
            this.lblFlowCalOutputFolder.TabIndex = 37;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(188, 586);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(102, 13);
            this.label15.TabIndex = 38;
            this.label15.Text = "Output File Location";
            // 
            // grpWRecord
            // 
            this.grpWRecord.Controls.Add(this.rdoWNo);
            this.grpWRecord.Controls.Add(this.rdoWYes);
            this.grpWRecord.Location = new System.Drawing.Point(828, 227);
            this.grpWRecord.Name = "grpWRecord";
            this.grpWRecord.Size = new System.Drawing.Size(140, 46);
            this.grpWRecord.TabIndex = 39;
            this.grpWRecord.TabStop = false;
            this.grpWRecord.Text = "Has W record";
            // 
            // rdoWNo
            // 
            this.rdoWNo.AutoSize = true;
            this.rdoWNo.Location = new System.Drawing.Point(75, 19);
            this.rdoWNo.Name = "rdoWNo";
            this.rdoWNo.Size = new System.Drawing.Size(39, 17);
            this.rdoWNo.TabIndex = 1;
            this.rdoWNo.Text = "No";
            this.rdoWNo.UseVisualStyleBackColor = true;
            // 
            // rdoWYes
            // 
            this.rdoWYes.AutoSize = true;
            this.rdoWYes.Checked = true;
            this.rdoWYes.Location = new System.Drawing.Point(15, 19);
            this.rdoWYes.Name = "rdoWYes";
            this.rdoWYes.Size = new System.Drawing.Size(43, 17);
            this.rdoWYes.TabIndex = 0;
            this.rdoWYes.TabStop = true;
            this.rdoWYes.Text = "Yes";
            this.rdoWYes.UseVisualStyleBackColor = true;
            // 
            // cbCopperStrip
            // 
            this.cbCopperStrip.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbCopperStrip.FormattingEnabled = true;
            this.cbCopperStrip.Items.AddRange(new object[] {
            "None",
            "1a",
            "1b",
            "2a",
            "2b",
            "2c",
            "2d",
            "2e",
            "3a",
            "3b",
            "4a",
            "4b",
            "4c"});
            this.cbCopperStrip.Location = new System.Drawing.Point(337, 246);
            this.cbCopperStrip.Name = "cbCopperStrip";
            this.cbCopperStrip.Size = new System.Drawing.Size(181, 21);
            this.cbCopperStrip.TabIndex = 41;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(175, 226);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(69, 13);
            this.label2.TabIndex = 42;
            this.label2.Text = "Saybolt Color";
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(383, 226);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(65, 13);
            this.label16.TabIndex = 43;
            this.label16.Text = "Copper Strip";
            // 
            // mseH2S
            // 
            this.mseH2S.Location = new System.Drawing.Point(609, 190);
            this.mseH2S.Mask = "0000.0000";
            this.mseH2S.Name = "mseH2S";
            this.mseH2S.Size = new System.Drawing.Size(59, 20);
            this.mseH2S.TabIndex = 44;
            this.mseH2S.Text = "00000000";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(586, 171);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(126, 13);
            this.label17.TabIndex = 45;
            this.label17.Text = "Concentration H2S (ppm)";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1010, 610);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.mseH2S);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cbCopperStrip);
            this.Controls.Add(this.cbSayboltColor);
            this.Controls.Add(this.grpWRecord);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.lblFlowCalOutputFolder);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.txtSourceAnalysisNumber);
            this.Controls.Add(this.grpGasLiq);
            this.Controls.Add(this.grpHasExtendedAnalysis);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.txtInstrumentNumber);
            this.Controls.Add(this.dtpSampleDateTime);
            this.Controls.Add(this.dtpFromdate);
            this.Controls.Add(this.cmdBuildFlowCalFile);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.txtOfficeComments);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.txtFieldComments);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtSampleTemperature);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtSamplePressure);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtCylinderNumber);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtSampleID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtSampleNumber);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtAnalysisTechnician);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtSampleTechnician);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Flow_Cal_Commit";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpHasExtendedAnalysis.ResumeLayout(false);
            this.grpHasExtendedAnalysis.PerformLayout();
            this.grpGasLiq.ResumeLayout(false);
            this.grpGasLiq.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.grpWRecord.ResumeLayout(false);
            this.grpWRecord.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtSampleTechnician;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtAnalysisTechnician;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtSampleNumber;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtSampleID;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtCylinderNumber;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtSamplePressure;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSampleTemperature;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoAbsolute;
        private System.Windows.Forms.RadioButton rdoGauge;
        private System.Windows.Forms.TextBox txtFieldComments;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtOfficeComments;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Button cmdBuildFlowCalFile;
        private System.Windows.Forms.DateTimePicker dtpFromdate;
        private System.Windows.Forms.DateTimePicker dtpSampleDateTime;
        private System.Windows.Forms.TextBox txtInstrumentNumber;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.GroupBox grpHasExtendedAnalysis;
        private System.Windows.Forms.RadioButton rdoExtNo;
        private System.Windows.Forms.RadioButton rdoExtYes;
        private System.Windows.Forms.GroupBox grpGasLiq;
        private System.Windows.Forms.RadioButton rdoLiquid;
        private System.Windows.Forms.RadioButton rdoGas;
        private System.Windows.Forms.TextBox txtSourceAnalysisNumber;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem selectOutputFileLocationToolStripMenuItem;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Label lblFlowCalOutputFolder;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.GroupBox grpWRecord;
        private System.Windows.Forms.RadioButton rdoWNo;
        private System.Windows.Forms.RadioButton rdoWYes;
        private System.Windows.Forms.ComboBox cbCopperStrip;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.MaskedTextBox mseH2S;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox cbSayboltColor;
    }
}

