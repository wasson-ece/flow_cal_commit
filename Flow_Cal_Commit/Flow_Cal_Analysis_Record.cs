﻿using System; 
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using Microsoft.Office.Core;
using Microsoft.Office.Interop.Excel;

namespace Flow_Cal_Commit
{
    [Serializable()]
    public class Flow_Cal_Analysis_Record
    {
        public string SourceAnalysisNumber{ get; set; }
        public string ColumnType{ get; set; }
        public string FromDate{ get; set; }
        public string ToDate{ get; set; }
        public string SampleDate{ get; set; }
        public string BTUBase{ get; set; }
        public string HeatingValue{ get; set; }
        public string RelativeDensity{ get; set; }
        public string Viscosity = "\t";
        public string SpecificHeatRatio = "\t";
        public string PressureBase{ get; set; }
        public string molpercentCO2{ get; set; }
        public string molpercentN2{ get; set; }
        public string molpercentMethane{ get; set; }
        public string molpercentEthane{ get; set; }
        public string molpercentPropane{ get; set; }
        public string molpercentIsobutane{ get; set; }
        public string molpercentNButane{ get; set; }
        public string molpercentIsopentane{ get; set; }
        public string molpercentNPentane{ get; set; }
        public string molpercentneoPentane{ get; set; }
        public string molpercentHexane{ get; set; }
        public string molpercentHeptane{ get; set; }
        public string molpercentOctane{ get; set; }
        public string molpercentNonane{ get; set; }
        public string molpercentDecane{ get; set; }
        public string molpercentArgon{ get; set; }
        public string molpercentCO{ get; set; }
        public string molpercentH2{ get; set; }
        public string molpercentO2{ get; set; }
        public string molpercentH2O{ get; set; }
        public string molpercentH2S{ get; set; }
        public string molpercentHe{ get; set; }
        public string PPMH2S = "\t";
        public string PPMCOS = "\t";
        public string PPMMeSH = "\t";
        public string PPMMES = "\t";
        public string PPMEtSH = "\t";
        public string PPMDMS = "\t";
        public string PPMSulfurs = "\t";
        public string LbpermillioncuftH2O = "\t";
        public string GPMMeasured{ get; set; }
        public string Samplenumber{ get; set; }
        public string HeatingValue1 { get; set; }
        public string GPMCondensate{ get; set; }
        public string BTUC6plus{ get; set; }
        public string FieldRemarks{ get; set; }
        public string OfficeRemarks{ get; set; }
        public string SampleTechnician{ get; set; }
        public string AnalysisTechnician{ get; set; }
        public string InstrumentNumber{ get; set; }
        public string SamplePressure{ get; set; }
        public string SampleTemperature{ get; set; }
        public string userdefinednumber1 = "\t";
        public string userdefinednumber2 = "\t";
        public string userdefinednumber3 = "\t";
        public string userdefinednumber4 = "\t";
        public string userdefinednumber5 = "\t";
        public string userdefinedstring1 = "\t";
        public string userdefinedstring2 = "\t";
        public string userdefinedstring3 = "\t";
        public string userdefinedstring4 = "\t";
        public string userdefinedstring5 = "\t";
        public string WellStreamfactor = "\t";
        public string EthaneGPM{ get; set; }
        public string PropaneGPM{ get; set; }
        public string IsobutaneGPM{ get; set; }
        public string NbutaneGPM{ get; set; }
        public string IsopentaneGPM{ get; set; }
        public string NpentaneGPM{ get; set; }
        public string HexaneGPM{ get; set; }
        public string HeptaneGPM{ get; set; }
        public string BTUSat{ get; set; }
        public string BTUSatAsDelivered{ get; set; }
        public string AnalysisDate{ get; set; }
        public string CylinderNumber{ get; set; }
        public string RecordStatus = "A\t";
        public string SamplePressureGuage{ get; set; }
        public string AtmosphericPressure{ get; set; }
        public string OctaneGPM{ get; set; }
        public string NonaneGPM{ get; set; }
        public string DecaneGPM{ get; set; }
        public string twentysixpsigasoline = "\t";
        public string totalmolpercent = "\t";
        public string UnNormalizedTotal{ get; set; }
        public string BTUperLBMass{ get; set; }
        public string NetWobbeIndex = "\t";
        public string GrossWobbeIndex{ get; set; }
        public string HCDewpoint = "\t";
        public string FieldHDCP = "\t";
        public string Cricondentherm = "\t";
        public string GrossBTUperCuFt{ get; set; }
        public string GrossBTUSatperCuFt{ get; set; }
        public string GrossBTUperCuFtAsDelivered{ get; set; }
        public string SPG{ get; set; }
        public string NetBTUrealperCuFt{ get; set; }
        public string NetBTUsatrealperCuFt = "\t";
        public string NetBTUrealAsDelivered = "\t";
        public string TemperatureBase{ get; set; }
        public string H2ODewpoint = "\t";
        public string TotalMassPercent = "\t";
        public string userdefinednumber6 = "\t";
        public string HasExtendedAnalysis{ get; set; }
        public string AnalysisType{ get; set; }
        public string FreeWater = "\t";
        public string Condensate = "\t";
        public string CompressibilityBase = "\t";
        public string CompressibilitySample{ get; set; }
        public string MolecularWeightSample{ get; set; }
        public string AbsoluteDensity{ get; set; }
        public string RelativeDensityLiq{ get; set; }
        public string APIGravity{ get; set; }
        public string GasLiquidVolumeRatio{ get; set; }
        public string MolWtOrLiqVol{ get; set; }
        public string ProductCode = "ETHYLENE\t";
        public string SampleID{ get; set; }
        public string molpercentEthylene { get; set; }
        public string molpercentPropylene { get; set; }
        public string molpercentButylenes { get; set; }
        public string BasedensityCalcMethod = "H\t";
        public string DensityByRange = "N\t";
        public string AnalysisCertificateNumber = "\t";
        public string AbsoluteDensityLiqAir = "\t";
        public string SampleCalcMethod = "\t";
        public string VaporPressure{ get; set; }
        public string DensityOffset = "\t";
        public string EVP100 = "\t";
        public string WtpercentMethane{ get; set; }
        public string WtpercentEthane{ get; set; }
        public string WtpercentPropane{ get; set; }
        public string WtpercentIsobutane{ get; set; }
        public string WtpercentNbutane{ get; set; }
        public string WtpercentIsopentane{ get; set; }
        public string WtpercentNpentane{ get; set; }
        public string WtpercentNeopentane{ get; set; }
        public string WtpercentHexane{ get; set; }
        public string WtpercentHeptane{ get; set; }
        public string WtpercentOctane{ get; set; }
        public string WtpercentNonane{ get; set; }
        public string WtpercentDecane{ get; set; }
        public string WtpercentCO2{ get; set; }
        public string WtpercentN2{ get; set; }
        public string WtpercentCO{ get; set; }
        public string WtpercentO2{ get; set; }
        public string WtpercentHe{ get; set; }
        public string WtpercentAr{ get; set; }
        public string WtpercentH2O{ get; set; }
        public string WtpercentH2S{ get; set; }
        public string WtpercentH2{ get; set; }
        public string WtpercentEthylene{ get; set; }
        public string WtpercentPropylene{ get; set; }
        public string WtpercentButylene{ get; set; }
        public string LVpercentMethane{ get; set; }
        public string LVpercentEthane{ get; set; }
        public string LVpercentPropane{ get; set; }
        public string LVpercentIsobutane{ get; set; }
        public string LVpercentNbutane{ get; set; }
        public string LVpercentIsopentane{ get; set; }
        public string LVpercentNpentane{ get; set; }
        public string LVpercentNeopentane{ get; set; }
        public string LVpercentHexane{ get; set; }
        public string LVpercentHeptane{ get; set; }
        public string LVpercentOctane{ get; set; }
        public string LVpercentNonane{ get; set; }
        public string LVpercentDecane{ get; set; }
        public string LVpercentCO2{ get; set; }
        public string LVpercentN2{ get; set; }
        public string LVpercentCO{ get; set; }
        public string LVpercentO2{ get; set; }
        public string LVpercentHe{ get; set; }
        public string LVpercentAr{ get; set; }
        public string LVpercentH2O{ get; set; }
        public string LVpercentH2S{ get; set; }
        public string LVpercentH2{ get; set; }
        public string LVpercentEthylene{ get; set; }
        public string LVpercentPropylene{ get; set; }
        public string LVpercentButylene{ get; set; }
        public string molpercentAromatics = "\t";
        public string WtpercentAromatics = "\t";
        public string LVpercentAromatics = "\t";
    }

}
