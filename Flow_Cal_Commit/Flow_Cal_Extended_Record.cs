﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flow_Cal_Commit
{
    [Serializable()]
    public class Flow_Cal_Extended_Record
    {
        public string SourceAnalysisNumber { get; set; }
        public string ColumnType = "X\t";
        public string Date { get; set; }
        public string Unused = "\t";
        public string Water { get; set; }
        public string CarbonMonoxide { get; set; }
        public string Argon { get; set; }
        public string Oxygen { get; set; }
        public string Helium { get; set; }
        public string Hydrogen { get; set; }
        public string HydrogenSulfide { get; set; }
        public string Nitrogen { get; set; }
        public string Methane { get; set; }
        public string CarbonDioxide { get; set; }
        public string Ethane { get; set; }
        public string Propane { get; set; }
        public string isoButane { get; set; }
        public string nButane { get; set; }
        public string neoPentane { get; set; }
        public string isoPentane { get; set; }
        public string nPentane { get; set; }
        public string two2Dimethylbutane { get; set; }
        public string two3Dimethylbutane { get; set; }
        public string Cyclopentane { get; set; }
        public string twoMethylpentane { get; set; }
        public string threeMethylpentane { get; set; }
        public string nHexane { get; set; }
        public string two2Dimethylpentane { get; set; }
        public string Methylcyclopentane { get; set; }
        public string two4Dimethylpentane { get; set; }
        public string two23Trimethylbutane { get; set; }
        public string Benzene { get; set; }
        public string three3Dimethylpentane { get; set; }
        public string Cyclohexane { get; set; }
        public string twoMethylhexane { get; set; }
        public string two3Dimethylpentane { get; set; }
        public string one1Dimethylcyclopentane { get; set; }
        public string threeMethylhexane { get; set; }
        public string onet3Dimethylcyclopentane { get; set; }
        public string onec3Dimethylcyclopentane { get; set; }
        public string threeEthylpentane { get; set; }
        public string onet2Dimethylcyclopentane { get; set; }
        public string two24Trimethylpentane { get; set; }
        public string nHeptane { get; set; }
        public string Methylcyclohexane { get; set; }
        public string one13Trimethylcyclopentane { get; set; }
        public string two2Dimethylhexane { get; set; }
        public string onec2Dimethylcyclopentane { get; set; }
        public string two5Dimethylhexane { get; set; }
        public string two4Dimethylhexane { get; set; }
        public string Ethylcyclopentane { get; set; }
        public string two23Trimethylpentane { get; set; }
        public string onet2c4Trimethylcyclopentane { get; set; }
        public string three3Dimethylhexane { get; set; }
        public string onet2c3Trimethylcyclopentane { get; set; }
        public string two34Trimethylpentane { get; set; }
        public string two3Dimethylhexane { get; set; }
        public string Toluene { get; set; }
        public string one12Trimethylcyclopentane { get; set; }
        public string onec2t4Trimethylcyclopentane { get; set; }
        public string twoMethylheptane { get; set; }
        public string fourMethylheptane { get; set; }
        public string three4Dimethylhexane { get; set; }
        public string threeMethylheptane { get; set; }
        public string threeEthylhexane { get; set; }
        public string UnknownDimethylheptane1 = "\t";
        public string ct12Dimethylcyclohexane { get; set; }
        public string onec3Dimethylcyclohexane { get; set; }
        public string onec2t3Trimethylcyclopentane { get; set; }
        public string onet4Dimethylcyclohexane { get; set; }
        public string two25Trimethylhexane { get; set; }
        public string one1Dimethylcyclohexane { get; set; }
        public string oneMethylt3ethylcyclopentane { get; set; }
        public string oneMethylc3ethylcyclopentane { get; set; }
        public string oneMethylt2ethylcyclopentane { get; set; }
        public string two24Trimethylhexane { get; set; }
        public string oneMethyl1ethylcyclopentane { get; set; }
        public string Cycloheptane { get; set; }
        public string MethylEthylcyclopentane { get; set; }
        public string nOctane { get; set; }
        public string onet2Dimethylcyclohexane { get; set; }
        public string two244Tetramethylpentane { get; set; }
        public string onet3Dimethylcyclohexane { get; set; }
        public string onec4Dimethylcyclohexane { get; set; }
        public string onec2c3Trimethylcyclopentane { get; set; }
        public string two44Trimethylhexane { get; set; }
        public string UnknownDimethylheptane2 = "\t";
        public string isopropylcyclopentane { get; set; }
        public string two35Trimethylhexane { get; set; }
        public string UnknownDimethylheptane3 = "\t";
        public string two2Dimethylheptane { get; set; }
        public string two4Dimethylheptane { get; set; }
        public string oneEthylc2methylcyclopentane { get; set; }
        public string two23Trimethylhexane { get; set; }
        public string onec2Dimethylcyclohexane { get; set; }
        public string two6Dimethylheptane { get; set; }
        public string nPropylcyclopentane { get; set; }
        public string onec3c5Trimethylcyclohexane { get; set; }
        public string Ethylcyclohexane { get; set; }
        public string two5Dimethylheptane { get; set; }
        public string UnknownDimethylheptane4 = "\t";
        public string three5Dimethylheptane { get; set; }
        public string UnknownDimethylheptane5 = "\t";
        public string ct13Dimethylcyclohexane { get; set; }
        public string UnknownTrimethylhexane = "\t";
        public string one13Trimethylcyclohexane { get; set; }
        public string two33Trimethylhexane { get; set; }
        public string three3Dimethylheptane { get; set; }
        public string one14Trimethylcyclohexane { get; set; }
        public string two233Tetramethylpentane { get; set; }
        public string Ethylbenzene { get; set; }
        public string two34Trimethylhexane { get; set; }
        public string onet2t4Trimethylcyclohexane { get; set; }
        public string two3Dimethylheptane { get; set; }
        public string onec3t5Trimethylcyclohexane { get; set; }
        public string UnknownDimethylheptane6 = "\t";
        public string mXylene { get; set; }
        public string pXylene { get; set; }
        public string three4Dimethylheptane { get; set; }
        public string twoMethyloctane { get; set; }
        public string fourMethyloctane { get; set; }
        public string UnknownTrimethylcyclohexane1 = "\t";
        public string threeMethyloctane { get; set; }
        public string UnknownTrimethylcyclohexane2 = "\t";
        public string UnknownTrimethylcyclohexane3 = "\t";
        public string onet2c3Trimethylcyclohexane { get; set; }
        public string onet2c4Trimethylcyclohexane { get; set; }
        public string oXylene { get; set; }
        public string UnknownTrimethylcyclohexane4 = "\t";
        public string one12Trimethylcyclohexane { get; set; }
        public string UnknownTrimethylcyclohexane5 = "\t";
        public string onec2t4Trimethylcyclohexane { get; set; }
        public string onec2c4Trimethylcyclohexane { get; set; }
        public string UnknownTrimethylcyclohexane6 = "\t";
        public string Isobutylcyclopentane { get; set; }
        public string nNonane { get; set; }
        public string two55Trimethylheptane { get; set; }
        public string onec2t3Trimethylcyclohexane { get; set; }
        public string onec2c3Trimethylcyclohexane { get; set; }
        public string Isopropylbenzene { get; set; }
        public string two2Dimethyloctane { get; set; }
        public string Isopropylcyclohexane { get; set; }
        public string Cyclooctane { get; set; }
        public string nButylcyclopentane { get; set; }
        public string nPropylcyclohexane { get; set; }
        public string three3Dimethyloctane { get; set; }
        public string nPropylbenzene { get; set; }
        public string oneMethyl3Ethylbenzene { get; set; }
        public string oneMethyl4Ethylbenzene { get; set; }
        public string UnknownC10Paraffin = "\t";
        public string three6Dimethyloctane { get; set; }
        public string mEthyltoluene { get; set; }
        public string pEthyltoluene { get; set; }
        public string two3Dimethyloctane { get; set; }
        public string fourMethylnonane { get; set; }
        public string fiveMethylnonane { get; set; }
        public string onethree5Trimethylbenzene { get; set; }
        public string twoMethylnonane { get; set; }
        public string threeEthyloctane { get; set; }
        public string threeMethylnonane { get; set; }
        public string oEthyltoluene { get; set; }
        public string one24Trimethylbenzene { get; set; }
        public string tertButylbenzene { get; set; }
        public string Methylcyclooctane { get; set; }
        public string tertButylcyclohexane { get; set; }
        public string Isobutylcyclohexane { get; set; }
        public string nDecane { get; set; }
        public string UnknownC10A = "\t";
        public string Isobutylbenzene { get; set; }
        public string secButylbenzene { get; set; }
        public string oneMethyl3isopropylbenzene { get; set; }
        public string one23Trimethylbenzene { get; set; }
        public string oneMethyl4isopropylbenzene { get; set; }
        public string oneMethyl2isopropylbenzene { get; set; }
        public string nButylcyclohexane { get; set; }
        public string one3Diethylbenzene { get; set; }
        public string oneMethyl3propylbenzene { get; set; }
        public string one2Diethylbenzene { get; set; }
        public string nButylbenzene { get; set; }
        public string oneMethyl4propylbenzene { get; set; }
        public string one4Diethylbenzene { get; set; }
        public string oneMethyl2propylbenzene { get; set; }
        public string one4Dimethyl2ethylbenzene { get; set; }
        public string one2Dimethyl4ethylbenzene { get; set; }
        public string one3Dimethyl2ethylbenzene { get; set; }
        public string one2Dimethyl3ethylbenzene { get; set; }
        public string nUndecane { get; set; }
        public string twoMethylbutylbenzene { get; set; }
        public string one245Tetramethylbenzene { get; set; }
        public string one2three5Tetramethylbenzene { get; set; }
        public string onetertButyl2methylbenzene { get; set; }
        public string one2three4Tetramethylbenzene { get; set; }
        public string Cyclodecane { get; set; }
        public string nPentylbenzene { get; set; }
        public string UnknownC121 = "\t";
        public string UnknownC122 = "\t";
        public string UnknownC123 = "\t";
        public string UnknownC124 = "\t";
        public string Naphthalene { get; set; }
        public string nDodecane { get; set; }
        public string oneMethylt24methylpentylcyclopentane { get; set; }
        public string onetertButylthree5dimethylbenzene { get; set; }
        public string one24Triethylbenzene { get; set; }
        public string onethree5Triethylbenzene { get; set; }
        public string nHexylbenzene { get; set; }
        public string nTridecane { get; set; }
        public string nTetradecane { get; set; }
        public string nPentadecane { get; set; }
        public string Ethylene { get; set; }
        public string Propylene { get; set; }
        public string Butylenes { get; set; }
    }
}
